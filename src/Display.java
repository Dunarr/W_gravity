import javax.swing.*;
import java.awt.*;

public class Display extends JPanel {
    public Balle[] balles = new Balle[15];
    public static int trainee = 255;
    public static float step = 0.04f;

    public void paintComponent(Graphics g) {
        g.setColor(new Color(255,255,255,Display.trainee));
        g.fillRect(0, 0, this.getWidth(), this.getHeight());
        g.setColor(Color.BLACK);
        g.fillRect(0, this.getHeight()-20, this.getWidth(), this.getHeight());
        for (int j = 0; j < this.balles.length; j++) {
            if (balles[j] != null) {
                //balles[j].display(g,this);
                balles[j].nextStep(this);
                g.setColor(balles[j].getColor());
                g.fillOval(balles[j].getX(), (int) balles[j].getPosition().getY(), balles[j].getSize(), balles[j].getSize());
            }
        }

    }

    public boolean addBalle(Balle balle) {
        for (int i = 0; i < this.balles.length; i++) {
            if (this.balles[i] == null) {
                this.balles[i] = balle;
                return true;
            }
        }
        return false;
    }

}
