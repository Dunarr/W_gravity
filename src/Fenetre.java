import javax.swing.*;
        import java.awt.*;

public class Fenetre extends JFrame {
    public Display display = new Display();
    public static Acceleration gravity = new Acceleration(0,-12);

    public Fenetre(){
        this.setTitle("BOING!");
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setSize(screenSize.width, screenSize.height-50);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setContentPane(display);
        this.setVisible(true);
    }
    public void launch(){
        while (true) {
            this.display.repaint();
            try {
                Thread.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
